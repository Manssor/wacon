export interface IWaterConsumptionDaily {
  id?: number;
  toilet?: number;
  shower?: number;
  dishwater?: number;
  washing?: number;
  faucet?: number;
  bath?: number;
  houseTotal?: number;
}

export class WaterConsumptionDaily implements IWaterConsumptionDaily {
  constructor(
    public id?: number,
    public toilet?: number,
    public shower?: number,
    public dishwater?: number,
    public washing?: number,
    public faucet?: number,
    public bath?: number,
    public houseTotal?: number
  ) {}
}
