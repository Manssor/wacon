import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WaconSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [WaconSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [WaconSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WaconSharedModule {
  static forRoot() {
    return {
      ngModule: WaconSharedModule
    };
  }
}
