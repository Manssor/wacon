import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { WaconButtonDemoModule } from './buttons/button/buttondemo.module';
import { WaconSplitbuttonDemoModule } from './buttons/splitbutton/splitbuttondemo.module';

import { WaconDialogDemoModule } from './overlay/dialog/dialogdemo.module';
import { WaconConfirmDialogDemoModule } from './overlay/confirmdialog/confirmdialogdemo.module';
import { WaconLightboxDemoModule } from './overlay/lightbox/lightboxdemo.module';
import { WaconTooltipDemoModule } from './overlay/tooltip/tooltipdemo.module';
import { WaconOverlayPanelDemoModule } from './overlay/overlaypanel/overlaypaneldemo.module';
import { WaconSideBarDemoModule } from './overlay/sidebar/sidebardemo.module';

import { WaconKeyFilterDemoModule } from './inputs/keyfilter/keyfilterdemo.module';
import { WaconInputTextDemoModule } from './inputs/inputtext/inputtextdemo.module';
import { WaconInputTextAreaDemoModule } from './inputs/inputtextarea/inputtextareademo.module';
import { WaconInputGroupDemoModule } from './inputs/inputgroup/inputgroupdemo.module';
import { WaconCalendarDemoModule } from './inputs/calendar/calendardemo.module';
import { WaconCheckboxDemoModule } from './inputs/checkbox/checkboxdemo.module';
import { WaconChipsDemoModule } from './inputs/chips/chipsdemo.module';
import { WaconColorPickerDemoModule } from './inputs/colorpicker/colorpickerdemo.module';
import { WaconInputMaskDemoModule } from './inputs/inputmask/inputmaskdemo.module';
import { WaconInputSwitchDemoModule } from './inputs/inputswitch/inputswitchdemo.module';
import { WaconPasswordIndicatorDemoModule } from './inputs/passwordindicator/passwordindicatordemo.module';
import { WaconAutoCompleteDemoModule } from './inputs/autocomplete/autocompletedemo.module';
import { WaconSliderDemoModule } from './inputs/slider/sliderdemo.module';
import { WaconSpinnerDemoModule } from './inputs/spinner/spinnerdemo.module';
import { WaconRatingDemoModule } from './inputs/rating/ratingdemo.module';
import { WaconSelectDemoModule } from './inputs/select/selectdemo.module';
import { WaconSelectButtonDemoModule } from './inputs/selectbutton/selectbuttondemo.module';
import { WaconListboxDemoModule } from './inputs/listbox/listboxdemo.module';
import { WaconRadioButtonDemoModule } from './inputs/radiobutton/radiobuttondemo.module';
import { WaconToggleButtonDemoModule } from './inputs/togglebutton/togglebuttondemo.module';
import { WaconEditorDemoModule } from './inputs/editor/editordemo.module';

import { WaconMessagesDemoModule } from './messages/messages/messagesdemo.module';
import { WaconToastDemoModule } from './messages/toast/toastdemo.module';
import { WaconGalleriaDemoModule } from './multimedia/galleria/galleriademo.module';

import { WaconFileUploadDemoModule } from './fileupload/fileupload/fileuploaddemo.module';

import { WaconAccordionDemoModule } from './panel/accordion/accordiondemo.module';
import { WaconPanelDemoModule } from './panel/panel/paneldemo.module';
import { WaconTabViewDemoModule } from './panel/tabview/tabviewdemo.module';
import { WaconFieldsetDemoModule } from './panel/fieldset/fieldsetdemo.module';
import { WaconToolbarDemoModule } from './panel/toolbar/toolbardemo.module';
import { WaconScrollPanelDemoModule } from './panel/scrollpanel/scrollpaneldemo.module';
import { WaconCardDemoModule } from './panel/card/carddemo.module';
import { WaconFlexGridDemoModule } from './panel/flexgrid/flexgriddemo.module';

import { WaconTableDemoModule } from './data/table/tabledemo.module';
import { WaconVirtualScrollerDemoModule } from './data/virtualscroller/virtualscrollerdemo.module';
import { WaconPickListDemoModule } from './data/picklist/picklistdemo.module';
import { WaconOrderListDemoModule } from './data/orderlist/orderlistdemo.module';
import { WaconFullCalendarDemoModule } from './data/fullcalendar/fullcalendardemo.module';
import { WaconTreeDemoModule } from './data/tree/treedemo.module';
import { WaconTreeTableDemoModule } from './data/treetable/treetabledemo.module';
import { WaconPaginatorDemoModule } from './data/paginator/paginatordemo.module';
import { WaconGmapDemoModule } from './data/gmap/gmapdemo.module';
import { WaconOrgChartDemoModule } from './data/orgchart/orgchartdemo.module';
import { WaconCarouselDemoModule } from './data/carousel/carouseldemo.module';
import { WaconDataViewDemoModule } from './data/dataview/dataviewdemo.module';

import { WaconBarchartDemoModule } from './charts/barchart/barchartdemo.module';
import { WaconDoughnutchartDemoModule } from './charts/doughnutchart/doughnutchartdemo.module';
import { WaconLinechartDemoModule } from './charts/linechart/linechartdemo.module';
import { WaconPiechartDemoModule } from './charts/piechart/piechartdemo.module';
import { WaconPolarareachartDemoModule } from './charts/polarareachart/polarareachartdemo.module';
import { WaconRadarchartDemoModule } from './charts/radarchart/radarchartdemo.module';

import { WaconDragDropDemoModule } from './dragdrop/dragdrop/dragdropdemo.module';

import { WaconMenuDemoModule } from './menu/menu/menudemo.module';
import { WaconContextMenuDemoModule } from './menu/contextmenu/contextmenudemo.module';
import { WaconPanelMenuDemoModule } from './menu/panelmenu/panelmenudemo.module';
import { WaconStepsDemoModule } from './menu/steps/stepsdemo.module';
import { WaconTieredMenuDemoModule } from './menu/tieredmenu/tieredmenudemo.module';
import { WaconBreadcrumbDemoModule } from './menu/breadcrumb/breadcrumbdemo.module';
import { WaconMegaMenuDemoModule } from './menu/megamenu/megamenudemo.module';
import { WaconMenuBarDemoModule } from './menu/menubar/menubardemo.module';
import { WaconSlideMenuDemoModule } from './menu/slidemenu/slidemenudemo.module';
import { WaconTabMenuDemoModule } from './menu/tabmenu/tabmenudemo.module';

import { WaconBlockUIDemoModule } from './misc/blockui/blockuidemo.module';
import { WaconCaptchaDemoModule } from './misc/captcha/captchademo.module';
import { WaconDeferDemoModule } from './misc/defer/deferdemo.module';
import { WaconInplaceDemoModule } from './misc/inplace/inplacedemo.module';
import { WaconProgressBarDemoModule } from './misc/progressbar/progressbardemo.module';
import { WaconRTLDemoModule } from './misc/rtl/rtldemo.module';
import { WaconTerminalDemoModule } from './misc/terminal/terminaldemo.module';
import { WaconValidationDemoModule } from './misc/validation/validationdemo.module';
import { WaconProgressSpinnerDemoModule } from './misc/progressspinner/progressspinnerdemo.module';

@NgModule({
  imports: [
    WaconMenuDemoModule,
    WaconContextMenuDemoModule,
    WaconPanelMenuDemoModule,
    WaconStepsDemoModule,
    WaconTieredMenuDemoModule,
    WaconBreadcrumbDemoModule,
    WaconMegaMenuDemoModule,
    WaconMenuBarDemoModule,
    WaconSlideMenuDemoModule,
    WaconTabMenuDemoModule,

    WaconBlockUIDemoModule,
    WaconCaptchaDemoModule,
    WaconDeferDemoModule,
    WaconInplaceDemoModule,
    WaconProgressBarDemoModule,
    WaconInputMaskDemoModule,
    WaconRTLDemoModule,
    WaconTerminalDemoModule,
    WaconValidationDemoModule,

    WaconButtonDemoModule,
    WaconSplitbuttonDemoModule,

    WaconInputTextDemoModule,
    WaconInputTextAreaDemoModule,
    WaconInputGroupDemoModule,
    WaconCalendarDemoModule,
    WaconChipsDemoModule,
    WaconInputMaskDemoModule,
    WaconInputSwitchDemoModule,
    WaconPasswordIndicatorDemoModule,
    WaconAutoCompleteDemoModule,
    WaconSliderDemoModule,
    WaconSpinnerDemoModule,
    WaconRatingDemoModule,
    WaconSelectDemoModule,
    WaconSelectButtonDemoModule,
    WaconListboxDemoModule,
    WaconRadioButtonDemoModule,
    WaconToggleButtonDemoModule,
    WaconEditorDemoModule,
    WaconColorPickerDemoModule,
    WaconCheckboxDemoModule,
    WaconKeyFilterDemoModule,

    WaconMessagesDemoModule,
    WaconToastDemoModule,
    WaconGalleriaDemoModule,

    WaconFileUploadDemoModule,

    WaconAccordionDemoModule,
    WaconPanelDemoModule,
    WaconTabViewDemoModule,
    WaconFieldsetDemoModule,
    WaconToolbarDemoModule,
    WaconScrollPanelDemoModule,
    WaconCardDemoModule,
    WaconFlexGridDemoModule,

    WaconBarchartDemoModule,
    WaconDoughnutchartDemoModule,
    WaconLinechartDemoModule,
    WaconPiechartDemoModule,
    WaconPolarareachartDemoModule,
    WaconRadarchartDemoModule,

    WaconDragDropDemoModule,

    WaconDialogDemoModule,
    WaconConfirmDialogDemoModule,
    WaconLightboxDemoModule,
    WaconTooltipDemoModule,
    WaconOverlayPanelDemoModule,
    WaconSideBarDemoModule,

    WaconTableDemoModule,
    WaconDataViewDemoModule,
    WaconVirtualScrollerDemoModule,
    WaconFullCalendarDemoModule,
    WaconOrderListDemoModule,
    WaconPickListDemoModule,
    WaconTreeDemoModule,
    WaconTreeTableDemoModule,
    WaconPaginatorDemoModule,
    WaconOrgChartDemoModule,
    WaconGmapDemoModule,
    WaconCarouselDemoModule,
    WaconProgressSpinnerDemoModule
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WaconprimengModule {}
