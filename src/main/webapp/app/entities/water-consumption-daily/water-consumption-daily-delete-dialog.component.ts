import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';
import { WaterConsumptionDailyService } from './water-consumption-daily.service';

@Component({
  selector: 'jhi-water-consumption-daily-delete-dialog',
  templateUrl: './water-consumption-daily-delete-dialog.component.html'
})
export class WaterConsumptionDailyDeleteDialogComponent {
  waterConsumptionDaily: IWaterConsumptionDaily;

  constructor(
    protected waterConsumptionDailyService: WaterConsumptionDailyService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.waterConsumptionDailyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'waterConsumptionDailyListModification',
        content: 'Deleted an waterConsumptionDaily'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-water-consumption-daily-delete-popup',
  template: ''
})
export class WaterConsumptionDailyDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ waterConsumptionDaily }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(WaterConsumptionDailyDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.waterConsumptionDaily = waterConsumptionDaily;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/water-consumption-daily', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/water-consumption-daily', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
