import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';

@Component({
  selector: 'jhi-water-consumption-daily-detail',
  templateUrl: './water-consumption-daily-detail.component.html'
})
export class WaterConsumptionDailyDetailComponent implements OnInit {
  waterConsumptionDaily: IWaterConsumptionDaily;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ waterConsumptionDaily }) => {
      this.waterConsumptionDaily = waterConsumptionDaily;
    });
  }

  previousState() {
    window.history.back();
  }
}
