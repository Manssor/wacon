import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';

type EntityResponseType = HttpResponse<IWaterConsumptionDaily>;
type EntityArrayResponseType = HttpResponse<IWaterConsumptionDaily[]>;

@Injectable({ providedIn: 'root' })
export class WaterConsumptionDailyService {
  public resourceUrl = SERVER_API_URL + 'api/water-consumption-dailies';

  constructor(protected http: HttpClient) {}

  create(waterConsumptionDaily: IWaterConsumptionDaily): Observable<EntityResponseType> {
    return this.http.post<IWaterConsumptionDaily>(this.resourceUrl, waterConsumptionDaily, { observe: 'response' });
  }

  update(waterConsumptionDaily: IWaterConsumptionDaily): Observable<EntityResponseType> {
    return this.http.put<IWaterConsumptionDaily>(this.resourceUrl, waterConsumptionDaily, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWaterConsumptionDaily>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWaterConsumptionDaily[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
