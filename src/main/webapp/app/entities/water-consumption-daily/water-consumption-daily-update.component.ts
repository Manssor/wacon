import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IWaterConsumptionDaily, WaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';
import { WaterConsumptionDailyService } from './water-consumption-daily.service';

@Component({
  selector: 'jhi-water-consumption-daily-update',
  templateUrl: './water-consumption-daily-update.component.html'
})
export class WaterConsumptionDailyUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    toilet: [],
    shower: [],
    dishwater: [],
    washing: [],
    faucet: [],
    bath: [],
    houseTotal: []
  });

  constructor(
    protected waterConsumptionDailyService: WaterConsumptionDailyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ waterConsumptionDaily }) => {
      this.updateForm(waterConsumptionDaily);
    });
  }

  updateForm(waterConsumptionDaily: IWaterConsumptionDaily) {
    this.editForm.patchValue({
      id: waterConsumptionDaily.id,
      toilet: waterConsumptionDaily.toilet,
      shower: waterConsumptionDaily.shower,
      dishwater: waterConsumptionDaily.dishwater,
      washing: waterConsumptionDaily.washing,
      faucet: waterConsumptionDaily.faucet,
      bath: waterConsumptionDaily.bath,
      houseTotal: waterConsumptionDaily.houseTotal
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const waterConsumptionDaily = this.createFromForm();
    if (waterConsumptionDaily.id !== undefined) {
      this.subscribeToSaveResponse(this.waterConsumptionDailyService.update(waterConsumptionDaily));
    } else {
      this.subscribeToSaveResponse(this.waterConsumptionDailyService.create(waterConsumptionDaily));
    }
  }

  private createFromForm(): IWaterConsumptionDaily {
    return {
      ...new WaterConsumptionDaily(),
      id: this.editForm.get(['id']).value,
      toilet: this.editForm.get(['toilet']).value,
      shower: this.editForm.get(['shower']).value,
      dishwater: this.editForm.get(['dishwater']).value,
      washing: this.editForm.get(['washing']).value,
      faucet: this.editForm.get(['faucet']).value,
      bath: this.editForm.get(['bath']).value,
      houseTotal: this.editForm.get(['houseTotal']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWaterConsumptionDaily>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
