import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { WaconSharedModule } from 'app/shared';
import {
  WaterConsumptionDailyComponent,
  WaterConsumptionDailyDetailComponent,
  WaterConsumptionDailyUpdateComponent,
  WaterConsumptionDailyDeletePopupComponent,
  WaterConsumptionDailyDeleteDialogComponent,
  waterConsumptionDailyRoute,
  waterConsumptionDailyPopupRoute
} from './';

const ENTITY_STATES = [...waterConsumptionDailyRoute, ...waterConsumptionDailyPopupRoute];

@NgModule({
  imports: [WaconSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    WaterConsumptionDailyComponent,
    WaterConsumptionDailyDetailComponent,
    WaterConsumptionDailyUpdateComponent,
    WaterConsumptionDailyDeleteDialogComponent,
    WaterConsumptionDailyDeletePopupComponent
  ],
  entryComponents: [
    WaterConsumptionDailyComponent,
    WaterConsumptionDailyUpdateComponent,
    WaterConsumptionDailyDeleteDialogComponent,
    WaterConsumptionDailyDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WaconWaterConsumptionDailyModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
