import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { WaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';
import { WaterConsumptionDailyService } from './water-consumption-daily.service';
import { WaterConsumptionDailyComponent } from './water-consumption-daily.component';
import { WaterConsumptionDailyDetailComponent } from './water-consumption-daily-detail.component';
import { WaterConsumptionDailyUpdateComponent } from './water-consumption-daily-update.component';
import { WaterConsumptionDailyDeletePopupComponent } from './water-consumption-daily-delete-dialog.component';
import { IWaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';

@Injectable({ providedIn: 'root' })
export class WaterConsumptionDailyResolve implements Resolve<IWaterConsumptionDaily> {
  constructor(private service: WaterConsumptionDailyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWaterConsumptionDaily> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<WaterConsumptionDaily>) => response.ok),
        map((waterConsumptionDaily: HttpResponse<WaterConsumptionDaily>) => waterConsumptionDaily.body)
      );
    }
    return of(new WaterConsumptionDaily());
  }
}

export const waterConsumptionDailyRoute: Routes = [
  {
    path: '',
    component: WaterConsumptionDailyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'waconApp.waterConsumptionDaily.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WaterConsumptionDailyDetailComponent,
    resolve: {
      waterConsumptionDaily: WaterConsumptionDailyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'waconApp.waterConsumptionDaily.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WaterConsumptionDailyUpdateComponent,
    resolve: {
      waterConsumptionDaily: WaterConsumptionDailyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'waconApp.waterConsumptionDaily.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WaterConsumptionDailyUpdateComponent,
    resolve: {
      waterConsumptionDaily: WaterConsumptionDailyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'waconApp.waterConsumptionDaily.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const waterConsumptionDailyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WaterConsumptionDailyDeletePopupComponent,
    resolve: {
      waterConsumptionDaily: WaterConsumptionDailyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'waconApp.waterConsumptionDaily.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
