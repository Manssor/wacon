import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WaconSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { ChartModule } from 'primeng/primeng';

@NgModule({
  imports: [WaconSharedModule, ChartModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WaconHomeModule {}

// RouterModule.forRoot(primeng_STATES, { useHash: true })
