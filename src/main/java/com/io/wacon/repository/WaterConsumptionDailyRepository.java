package com.io.wacon.repository;

import com.io.wacon.domain.WaterConsumptionDaily;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WaterConsumptionDaily entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WaterConsumptionDailyRepository extends JpaRepository<WaterConsumptionDaily, Long> {

}
