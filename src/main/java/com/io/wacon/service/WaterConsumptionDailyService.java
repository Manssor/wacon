package com.io.wacon.service;

import com.io.wacon.domain.WaterConsumptionDaily;
import com.io.wacon.repository.WaterConsumptionDailyRepository;
import com.io.wacon.service.dto.WaterConsumptionDailyDTO;
import com.io.wacon.service.mapper.WaterConsumptionDailyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link WaterConsumptionDaily}.
 */
@Service
@Transactional
public class WaterConsumptionDailyService {

    private final Logger log = LoggerFactory.getLogger(WaterConsumptionDailyService.class);

    private final WaterConsumptionDailyRepository waterConsumptionDailyRepository;

    private final WaterConsumptionDailyMapper waterConsumptionDailyMapper;

    public WaterConsumptionDailyService(WaterConsumptionDailyRepository waterConsumptionDailyRepository, WaterConsumptionDailyMapper waterConsumptionDailyMapper) {
        this.waterConsumptionDailyRepository = waterConsumptionDailyRepository;
        this.waterConsumptionDailyMapper = waterConsumptionDailyMapper;
    }

    /**
     * Save a waterConsumptionDaily.
     *
     * @param waterConsumptionDailyDTO the entity to save.
     * @return the persisted entity.
     */
    public WaterConsumptionDailyDTO save(WaterConsumptionDailyDTO waterConsumptionDailyDTO) {
        log.debug("Request to save WaterConsumptionDaily : {}", waterConsumptionDailyDTO);
        WaterConsumptionDaily waterConsumptionDaily = waterConsumptionDailyMapper.toEntity(waterConsumptionDailyDTO);
        waterConsumptionDaily = waterConsumptionDailyRepository.save(waterConsumptionDaily);
        return waterConsumptionDailyMapper.toDto(waterConsumptionDaily);
    }

    /**
     * Get all the waterConsumptionDailies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<WaterConsumptionDailyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WaterConsumptionDailies");
        return waterConsumptionDailyRepository.findAll(pageable)
            .map(waterConsumptionDailyMapper::toDto);
    }


    /**
     * Get one waterConsumptionDaily by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<WaterConsumptionDailyDTO> findOne(Long id) {
        log.debug("Request to get WaterConsumptionDaily : {}", id);
        return waterConsumptionDailyRepository.findById(id)
            .map(waterConsumptionDailyMapper::toDto);
    }

    /**
     * Delete the waterConsumptionDaily by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete WaterConsumptionDaily : {}", id);
        waterConsumptionDailyRepository.deleteById(id);
    }
}
