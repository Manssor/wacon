package com.io.wacon.service.mapper;

import com.io.wacon.domain.*;
import com.io.wacon.service.dto.WaterConsumptionDailyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link WaterConsumptionDaily} and its DTO {@link WaterConsumptionDailyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WaterConsumptionDailyMapper extends EntityMapper<WaterConsumptionDailyDTO, WaterConsumptionDaily> {



    default WaterConsumptionDaily fromId(Long id) {
        if (id == null) {
            return null;
        }
        WaterConsumptionDaily waterConsumptionDaily = new WaterConsumptionDaily();
        waterConsumptionDaily.setId(id);
        return waterConsumptionDaily;
    }
}
