package com.io.wacon.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.io.wacon.domain.WaterConsumptionDaily} entity.
 */
public class WaterConsumptionDailyDTO implements Serializable {

    private Long id;

    private Long toilet;

    private Long shower;

    private Long dishwater;

    private Long washing;

    private Long faucet;

    private Long bath;

    private Long houseTotal;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getToilet() {
        return toilet;
    }

    public void setToilet(Long toilet) {
        this.toilet = toilet;
    }

    public Long getShower() {
        return shower;
    }

    public void setShower(Long shower) {
        this.shower = shower;
    }

    public Long getDishwater() {
        return dishwater;
    }

    public void setDishwater(Long dishwater) {
        this.dishwater = dishwater;
    }

    public Long getWashing() {
        return washing;
    }

    public void setWashing(Long washing) {
        this.washing = washing;
    }

    public Long getFaucet() {
        return faucet;
    }

    public void setFaucet(Long faucet) {
        this.faucet = faucet;
    }

    public Long getBath() {
        return bath;
    }

    public void setBath(Long bath) {
        this.bath = bath;
    }

    public Long getHouseTotal() {
        return houseTotal;
    }

    public void setHouseTotal(Long houseTotal) {
        this.houseTotal = houseTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WaterConsumptionDailyDTO waterConsumptionDailyDTO = (WaterConsumptionDailyDTO) o;
        if (waterConsumptionDailyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), waterConsumptionDailyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WaterConsumptionDailyDTO{" +
            "id=" + getId() +
            ", toilet=" + getToilet() +
            ", shower=" + getShower() +
            ", dishwater=" + getDishwater() +
            ", washing=" + getWashing() +
            ", faucet=" + getFaucet() +
            ", bath=" + getBath() +
            ", houseTotal=" + getHouseTotal() +
            "}";
    }
}
