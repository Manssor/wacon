package com.io.wacon.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A WaterConsumptionDaily.
 */
@Entity
@Table(name = "water_consumption_daily")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WaterConsumptionDaily implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "toilet")
    private Long toilet;

    @Column(name = "shower")
    private Long shower;

    @Column(name = "dishwater")
    private Long dishwater;

    @Column(name = "washing")
    private Long washing;

    @Column(name = "faucet")
    private Long faucet;

    @Column(name = "bath")
    private Long bath;

    @Column(name = "house_total")
    private Long houseTotal;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getToilet() {
        return toilet;
    }

    public WaterConsumptionDaily toilet(Long toilet) {
        this.toilet = toilet;
        return this;
    }

    public void setToilet(Long toilet) {
        this.toilet = toilet;
    }

    public Long getShower() {
        return shower;
    }

    public WaterConsumptionDaily shower(Long shower) {
        this.shower = shower;
        return this;
    }

    public void setShower(Long shower) {
        this.shower = shower;
    }

    public Long getDishwater() {
        return dishwater;
    }

    public WaterConsumptionDaily dishwater(Long dishwater) {
        this.dishwater = dishwater;
        return this;
    }

    public void setDishwater(Long dishwater) {
        this.dishwater = dishwater;
    }

    public Long getWashing() {
        return washing;
    }

    public WaterConsumptionDaily washing(Long washing) {
        this.washing = washing;
        return this;
    }

    public void setWashing(Long washing) {
        this.washing = washing;
    }

    public Long getFaucet() {
        return faucet;
    }

    public WaterConsumptionDaily faucet(Long faucet) {
        this.faucet = faucet;
        return this;
    }

    public void setFaucet(Long faucet) {
        this.faucet = faucet;
    }

    public Long getBath() {
        return bath;
    }

    public WaterConsumptionDaily bath(Long bath) {
        this.bath = bath;
        return this;
    }

    public void setBath(Long bath) {
        this.bath = bath;
    }

    public Long getHouseTotal() {
        return houseTotal;
    }

    public WaterConsumptionDaily houseTotal(Long houseTotal) {
        this.houseTotal = houseTotal;
        return this;
    }

    public void setHouseTotal(Long houseTotal) {
        this.houseTotal = houseTotal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WaterConsumptionDaily)) {
            return false;
        }
        return id != null && id.equals(((WaterConsumptionDaily) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "WaterConsumptionDaily{" +
            "id=" + getId() +
            ", toilet=" + getToilet() +
            ", shower=" + getShower() +
            ", dishwater=" + getDishwater() +
            ", washing=" + getWashing() +
            ", faucet=" + getFaucet() +
            ", bath=" + getBath() +
            ", houseTotal=" + getHouseTotal() +
            "}";
    }
}
