/**
 * View Models used by Spring MVC REST controllers.
 */
package com.io.wacon.web.rest.vm;
