package com.io.wacon.web.rest;

import com.io.wacon.service.WaterConsumptionDailyService;
import com.io.wacon.web.rest.errors.BadRequestAlertException;
import com.io.wacon.service.dto.WaterConsumptionDailyDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.io.wacon.domain.WaterConsumptionDaily}.
 */
@RestController
@RequestMapping("/api")
public class WaterConsumptionDailyResource {

    private final Logger log = LoggerFactory.getLogger(WaterConsumptionDailyResource.class);

    private static final String ENTITY_NAME = "waterConsumptionDaily";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WaterConsumptionDailyService waterConsumptionDailyService;

    public WaterConsumptionDailyResource(WaterConsumptionDailyService waterConsumptionDailyService) {
        this.waterConsumptionDailyService = waterConsumptionDailyService;
    }

    /**
     * {@code POST  /water-consumption-dailies} : Create a new waterConsumptionDaily.
     *
     * @param waterConsumptionDailyDTO the waterConsumptionDailyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new waterConsumptionDailyDTO, or with status {@code 400 (Bad Request)} if the waterConsumptionDaily has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/water-consumption-dailies")
    public ResponseEntity<WaterConsumptionDailyDTO> createWaterConsumptionDaily(@RequestBody WaterConsumptionDailyDTO waterConsumptionDailyDTO) throws URISyntaxException {
        log.debug("REST request to save WaterConsumptionDaily : {}", waterConsumptionDailyDTO);
        if (waterConsumptionDailyDTO.getId() != null) {
            throw new BadRequestAlertException("A new waterConsumptionDaily cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WaterConsumptionDailyDTO result = waterConsumptionDailyService.save(waterConsumptionDailyDTO);
        return ResponseEntity.created(new URI("/api/water-consumption-dailies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /water-consumption-dailies} : Updates an existing waterConsumptionDaily.
     *
     * @param waterConsumptionDailyDTO the waterConsumptionDailyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated waterConsumptionDailyDTO,
     * or with status {@code 400 (Bad Request)} if the waterConsumptionDailyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the waterConsumptionDailyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/water-consumption-dailies")
    public ResponseEntity<WaterConsumptionDailyDTO> updateWaterConsumptionDaily(@RequestBody WaterConsumptionDailyDTO waterConsumptionDailyDTO) throws URISyntaxException {
        log.debug("REST request to update WaterConsumptionDaily : {}", waterConsumptionDailyDTO);
        if (waterConsumptionDailyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WaterConsumptionDailyDTO result = waterConsumptionDailyService.save(waterConsumptionDailyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, waterConsumptionDailyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /water-consumption-dailies} : get all the waterConsumptionDailies.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of waterConsumptionDailies in body.
     */
    @GetMapping("/water-consumption-dailies")
    public ResponseEntity<List<WaterConsumptionDailyDTO>> getAllWaterConsumptionDailies(Pageable pageable) {
        log.debug("REST request to get a page of WaterConsumptionDailies");
        Page<WaterConsumptionDailyDTO> page = waterConsumptionDailyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /water-consumption-dailies/:id} : get the "id" waterConsumptionDaily.
     *
     * @param id the id of the waterConsumptionDailyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the waterConsumptionDailyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/water-consumption-dailies/{id}")
    public ResponseEntity<WaterConsumptionDailyDTO> getWaterConsumptionDaily(@PathVariable Long id) {
        log.debug("REST request to get WaterConsumptionDaily : {}", id);
        Optional<WaterConsumptionDailyDTO> waterConsumptionDailyDTO = waterConsumptionDailyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(waterConsumptionDailyDTO);
    }

    /**
     * {@code DELETE  /water-consumption-dailies/:id} : delete the "id" waterConsumptionDaily.
     *
     * @param id the id of the waterConsumptionDailyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/water-consumption-dailies/{id}")
    public ResponseEntity<Void> deleteWaterConsumptionDaily(@PathVariable Long id) {
        log.debug("REST request to delete WaterConsumptionDaily : {}", id);
        waterConsumptionDailyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
