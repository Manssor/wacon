/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { WaconTestModule } from '../../../test.module';
import { WaterConsumptionDailyDetailComponent } from 'app/entities/water-consumption-daily/water-consumption-daily-detail.component';
import { WaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';

describe('Component Tests', () => {
  describe('WaterConsumptionDaily Management Detail Component', () => {
    let comp: WaterConsumptionDailyDetailComponent;
    let fixture: ComponentFixture<WaterConsumptionDailyDetailComponent>;
    const route = ({ data: of({ waterConsumptionDaily: new WaterConsumptionDaily(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WaconTestModule],
        declarations: [WaterConsumptionDailyDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(WaterConsumptionDailyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(WaterConsumptionDailyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.waterConsumptionDaily).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
