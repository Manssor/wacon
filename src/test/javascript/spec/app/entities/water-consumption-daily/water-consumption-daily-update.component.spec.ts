/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { WaconTestModule } from '../../../test.module';
import { WaterConsumptionDailyUpdateComponent } from 'app/entities/water-consumption-daily/water-consumption-daily-update.component';
import { WaterConsumptionDailyService } from 'app/entities/water-consumption-daily/water-consumption-daily.service';
import { WaterConsumptionDaily } from 'app/shared/model/water-consumption-daily.model';

describe('Component Tests', () => {
  describe('WaterConsumptionDaily Management Update Component', () => {
    let comp: WaterConsumptionDailyUpdateComponent;
    let fixture: ComponentFixture<WaterConsumptionDailyUpdateComponent>;
    let service: WaterConsumptionDailyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [WaconTestModule],
        declarations: [WaterConsumptionDailyUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(WaterConsumptionDailyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WaterConsumptionDailyUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(WaterConsumptionDailyService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new WaterConsumptionDaily(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new WaterConsumptionDaily();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
