package com.io.wacon.web.rest;

import com.io.wacon.WaconApp;
import com.io.wacon.domain.WaterConsumptionDaily;
import com.io.wacon.repository.WaterConsumptionDailyRepository;
import com.io.wacon.service.WaterConsumptionDailyService;
import com.io.wacon.service.dto.WaterConsumptionDailyDTO;
import com.io.wacon.service.mapper.WaterConsumptionDailyMapper;
import com.io.wacon.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.io.wacon.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WaterConsumptionDailyResource} REST controller.
 */
@SpringBootTest(classes = WaconApp.class)
public class WaterConsumptionDailyResourceIT {

    private static final Long DEFAULT_TOILET = 1L;
    private static final Long UPDATED_TOILET = 2L;
    private static final Long SMALLER_TOILET = 1L - 1L;

    private static final Long DEFAULT_SHOWER = 1L;
    private static final Long UPDATED_SHOWER = 2L;
    private static final Long SMALLER_SHOWER = 1L - 1L;

    private static final Long DEFAULT_DISHWATER = 1L;
    private static final Long UPDATED_DISHWATER = 2L;
    private static final Long SMALLER_DISHWATER = 1L - 1L;

    private static final Long DEFAULT_WASHING = 1L;
    private static final Long UPDATED_WASHING = 2L;
    private static final Long SMALLER_WASHING = 1L - 1L;

    private static final Long DEFAULT_FAUCET = 1L;
    private static final Long UPDATED_FAUCET = 2L;
    private static final Long SMALLER_FAUCET = 1L - 1L;

    private static final Long DEFAULT_BATH = 1L;
    private static final Long UPDATED_BATH = 2L;
    private static final Long SMALLER_BATH = 1L - 1L;

    private static final Long DEFAULT_HOUSE_TOTAL = 1L;
    private static final Long UPDATED_HOUSE_TOTAL = 2L;
    private static final Long SMALLER_HOUSE_TOTAL = 1L - 1L;

    @Autowired
    private WaterConsumptionDailyRepository waterConsumptionDailyRepository;

    @Autowired
    private WaterConsumptionDailyMapper waterConsumptionDailyMapper;

    @Autowired
    private WaterConsumptionDailyService waterConsumptionDailyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWaterConsumptionDailyMockMvc;

    private WaterConsumptionDaily waterConsumptionDaily;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WaterConsumptionDailyResource waterConsumptionDailyResource = new WaterConsumptionDailyResource(waterConsumptionDailyService);
        this.restWaterConsumptionDailyMockMvc = MockMvcBuilders.standaloneSetup(waterConsumptionDailyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WaterConsumptionDaily createEntity(EntityManager em) {
        WaterConsumptionDaily waterConsumptionDaily = new WaterConsumptionDaily()
            .toilet(DEFAULT_TOILET)
            .shower(DEFAULT_SHOWER)
            .dishwater(DEFAULT_DISHWATER)
            .washing(DEFAULT_WASHING)
            .faucet(DEFAULT_FAUCET)
            .bath(DEFAULT_BATH)
            .houseTotal(DEFAULT_HOUSE_TOTAL);
        return waterConsumptionDaily;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WaterConsumptionDaily createUpdatedEntity(EntityManager em) {
        WaterConsumptionDaily waterConsumptionDaily = new WaterConsumptionDaily()
            .toilet(UPDATED_TOILET)
            .shower(UPDATED_SHOWER)
            .dishwater(UPDATED_DISHWATER)
            .washing(UPDATED_WASHING)
            .faucet(UPDATED_FAUCET)
            .bath(UPDATED_BATH)
            .houseTotal(UPDATED_HOUSE_TOTAL);
        return waterConsumptionDaily;
    }

    @BeforeEach
    public void initTest() {
        waterConsumptionDaily = createEntity(em);
    }

    @Test
    @Transactional
    public void createWaterConsumptionDaily() throws Exception {
        int databaseSizeBeforeCreate = waterConsumptionDailyRepository.findAll().size();

        // Create the WaterConsumptionDaily
        WaterConsumptionDailyDTO waterConsumptionDailyDTO = waterConsumptionDailyMapper.toDto(waterConsumptionDaily);
        restWaterConsumptionDailyMockMvc.perform(post("/api/water-consumption-dailies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(waterConsumptionDailyDTO)))
            .andExpect(status().isCreated());

        // Validate the WaterConsumptionDaily in the database
        List<WaterConsumptionDaily> waterConsumptionDailyList = waterConsumptionDailyRepository.findAll();
        assertThat(waterConsumptionDailyList).hasSize(databaseSizeBeforeCreate + 1);
        WaterConsumptionDaily testWaterConsumptionDaily = waterConsumptionDailyList.get(waterConsumptionDailyList.size() - 1);
        assertThat(testWaterConsumptionDaily.getToilet()).isEqualTo(DEFAULT_TOILET);
        assertThat(testWaterConsumptionDaily.getShower()).isEqualTo(DEFAULT_SHOWER);
        assertThat(testWaterConsumptionDaily.getDishwater()).isEqualTo(DEFAULT_DISHWATER);
        assertThat(testWaterConsumptionDaily.getWashing()).isEqualTo(DEFAULT_WASHING);
        assertThat(testWaterConsumptionDaily.getFaucet()).isEqualTo(DEFAULT_FAUCET);
        assertThat(testWaterConsumptionDaily.getBath()).isEqualTo(DEFAULT_BATH);
        assertThat(testWaterConsumptionDaily.getHouseTotal()).isEqualTo(DEFAULT_HOUSE_TOTAL);
    }

    @Test
    @Transactional
    public void createWaterConsumptionDailyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = waterConsumptionDailyRepository.findAll().size();

        // Create the WaterConsumptionDaily with an existing ID
        waterConsumptionDaily.setId(1L);
        WaterConsumptionDailyDTO waterConsumptionDailyDTO = waterConsumptionDailyMapper.toDto(waterConsumptionDaily);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWaterConsumptionDailyMockMvc.perform(post("/api/water-consumption-dailies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(waterConsumptionDailyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the WaterConsumptionDaily in the database
        List<WaterConsumptionDaily> waterConsumptionDailyList = waterConsumptionDailyRepository.findAll();
        assertThat(waterConsumptionDailyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllWaterConsumptionDailies() throws Exception {
        // Initialize the database
        waterConsumptionDailyRepository.saveAndFlush(waterConsumptionDaily);

        // Get all the waterConsumptionDailyList
        restWaterConsumptionDailyMockMvc.perform(get("/api/water-consumption-dailies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(waterConsumptionDaily.getId().intValue())))
            .andExpect(jsonPath("$.[*].toilet").value(hasItem(DEFAULT_TOILET.intValue())))
            .andExpect(jsonPath("$.[*].shower").value(hasItem(DEFAULT_SHOWER.intValue())))
            .andExpect(jsonPath("$.[*].dishwater").value(hasItem(DEFAULT_DISHWATER.intValue())))
            .andExpect(jsonPath("$.[*].washing").value(hasItem(DEFAULT_WASHING.intValue())))
            .andExpect(jsonPath("$.[*].faucet").value(hasItem(DEFAULT_FAUCET.intValue())))
            .andExpect(jsonPath("$.[*].bath").value(hasItem(DEFAULT_BATH.intValue())))
            .andExpect(jsonPath("$.[*].houseTotal").value(hasItem(DEFAULT_HOUSE_TOTAL.intValue())));
    }
    
    @Test
    @Transactional
    public void getWaterConsumptionDaily() throws Exception {
        // Initialize the database
        waterConsumptionDailyRepository.saveAndFlush(waterConsumptionDaily);

        // Get the waterConsumptionDaily
        restWaterConsumptionDailyMockMvc.perform(get("/api/water-consumption-dailies/{id}", waterConsumptionDaily.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(waterConsumptionDaily.getId().intValue()))
            .andExpect(jsonPath("$.toilet").value(DEFAULT_TOILET.intValue()))
            .andExpect(jsonPath("$.shower").value(DEFAULT_SHOWER.intValue()))
            .andExpect(jsonPath("$.dishwater").value(DEFAULT_DISHWATER.intValue()))
            .andExpect(jsonPath("$.washing").value(DEFAULT_WASHING.intValue()))
            .andExpect(jsonPath("$.faucet").value(DEFAULT_FAUCET.intValue()))
            .andExpect(jsonPath("$.bath").value(DEFAULT_BATH.intValue()))
            .andExpect(jsonPath("$.houseTotal").value(DEFAULT_HOUSE_TOTAL.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingWaterConsumptionDaily() throws Exception {
        // Get the waterConsumptionDaily
        restWaterConsumptionDailyMockMvc.perform(get("/api/water-consumption-dailies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWaterConsumptionDaily() throws Exception {
        // Initialize the database
        waterConsumptionDailyRepository.saveAndFlush(waterConsumptionDaily);

        int databaseSizeBeforeUpdate = waterConsumptionDailyRepository.findAll().size();

        // Update the waterConsumptionDaily
        WaterConsumptionDaily updatedWaterConsumptionDaily = waterConsumptionDailyRepository.findById(waterConsumptionDaily.getId()).get();
        // Disconnect from session so that the updates on updatedWaterConsumptionDaily are not directly saved in db
        em.detach(updatedWaterConsumptionDaily);
        updatedWaterConsumptionDaily
            .toilet(UPDATED_TOILET)
            .shower(UPDATED_SHOWER)
            .dishwater(UPDATED_DISHWATER)
            .washing(UPDATED_WASHING)
            .faucet(UPDATED_FAUCET)
            .bath(UPDATED_BATH)
            .houseTotal(UPDATED_HOUSE_TOTAL);
        WaterConsumptionDailyDTO waterConsumptionDailyDTO = waterConsumptionDailyMapper.toDto(updatedWaterConsumptionDaily);

        restWaterConsumptionDailyMockMvc.perform(put("/api/water-consumption-dailies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(waterConsumptionDailyDTO)))
            .andExpect(status().isOk());

        // Validate the WaterConsumptionDaily in the database
        List<WaterConsumptionDaily> waterConsumptionDailyList = waterConsumptionDailyRepository.findAll();
        assertThat(waterConsumptionDailyList).hasSize(databaseSizeBeforeUpdate);
        WaterConsumptionDaily testWaterConsumptionDaily = waterConsumptionDailyList.get(waterConsumptionDailyList.size() - 1);
        assertThat(testWaterConsumptionDaily.getToilet()).isEqualTo(UPDATED_TOILET);
        assertThat(testWaterConsumptionDaily.getShower()).isEqualTo(UPDATED_SHOWER);
        assertThat(testWaterConsumptionDaily.getDishwater()).isEqualTo(UPDATED_DISHWATER);
        assertThat(testWaterConsumptionDaily.getWashing()).isEqualTo(UPDATED_WASHING);
        assertThat(testWaterConsumptionDaily.getFaucet()).isEqualTo(UPDATED_FAUCET);
        assertThat(testWaterConsumptionDaily.getBath()).isEqualTo(UPDATED_BATH);
        assertThat(testWaterConsumptionDaily.getHouseTotal()).isEqualTo(UPDATED_HOUSE_TOTAL);
    }

    @Test
    @Transactional
    public void updateNonExistingWaterConsumptionDaily() throws Exception {
        int databaseSizeBeforeUpdate = waterConsumptionDailyRepository.findAll().size();

        // Create the WaterConsumptionDaily
        WaterConsumptionDailyDTO waterConsumptionDailyDTO = waterConsumptionDailyMapper.toDto(waterConsumptionDaily);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWaterConsumptionDailyMockMvc.perform(put("/api/water-consumption-dailies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(waterConsumptionDailyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the WaterConsumptionDaily in the database
        List<WaterConsumptionDaily> waterConsumptionDailyList = waterConsumptionDailyRepository.findAll();
        assertThat(waterConsumptionDailyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWaterConsumptionDaily() throws Exception {
        // Initialize the database
        waterConsumptionDailyRepository.saveAndFlush(waterConsumptionDaily);

        int databaseSizeBeforeDelete = waterConsumptionDailyRepository.findAll().size();

        // Delete the waterConsumptionDaily
        restWaterConsumptionDailyMockMvc.perform(delete("/api/water-consumption-dailies/{id}", waterConsumptionDaily.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WaterConsumptionDaily> waterConsumptionDailyList = waterConsumptionDailyRepository.findAll();
        assertThat(waterConsumptionDailyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WaterConsumptionDaily.class);
        WaterConsumptionDaily waterConsumptionDaily1 = new WaterConsumptionDaily();
        waterConsumptionDaily1.setId(1L);
        WaterConsumptionDaily waterConsumptionDaily2 = new WaterConsumptionDaily();
        waterConsumptionDaily2.setId(waterConsumptionDaily1.getId());
        assertThat(waterConsumptionDaily1).isEqualTo(waterConsumptionDaily2);
        waterConsumptionDaily2.setId(2L);
        assertThat(waterConsumptionDaily1).isNotEqualTo(waterConsumptionDaily2);
        waterConsumptionDaily1.setId(null);
        assertThat(waterConsumptionDaily1).isNotEqualTo(waterConsumptionDaily2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WaterConsumptionDailyDTO.class);
        WaterConsumptionDailyDTO waterConsumptionDailyDTO1 = new WaterConsumptionDailyDTO();
        waterConsumptionDailyDTO1.setId(1L);
        WaterConsumptionDailyDTO waterConsumptionDailyDTO2 = new WaterConsumptionDailyDTO();
        assertThat(waterConsumptionDailyDTO1).isNotEqualTo(waterConsumptionDailyDTO2);
        waterConsumptionDailyDTO2.setId(waterConsumptionDailyDTO1.getId());
        assertThat(waterConsumptionDailyDTO1).isEqualTo(waterConsumptionDailyDTO2);
        waterConsumptionDailyDTO2.setId(2L);
        assertThat(waterConsumptionDailyDTO1).isNotEqualTo(waterConsumptionDailyDTO2);
        waterConsumptionDailyDTO1.setId(null);
        assertThat(waterConsumptionDailyDTO1).isNotEqualTo(waterConsumptionDailyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(waterConsumptionDailyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(waterConsumptionDailyMapper.fromId(null)).isNull();
    }
}
